<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 14.05.2018
 * Time: 13:40
 */

namespace App\Util;


use Pagerfanta\Exception\LogicException;
use Pagerfanta\Pagerfanta;

class PagerData
{
    public static function getPageData(Pagerfanta $page): array
    {
        $data = [];
        try{
            $data['previousPage'] = $page->getPreviousPage();
        } catch (LogicException $e) {
            $data['previousPage'] = null;
        }
        try{
            $data['nextPage'] = $page->getNextPage();
        } catch (LogicException $e) {
            $data['nextPage'] = null;
        }
        $data['lastPage'] = $page->getNbPages();
        return $data;
    }

}