<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 12.06.2018
 * Time: 17:22
 */

namespace App\Util;


class HideEmail
{
    public static function hide(string $email): string
    {
        $parted = explode('@', $email);
        return explode('@', $email)[0][0] . '***' . substr($parted[0], -1) . '@' . $parted[1];
    }
}