<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 19.06.2018
 * Time: 17:37
 */

namespace App\Util;


use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

class UserGroups
{
    /**
     * @var ArrayCollection|User
     */
    private $users;

    /**
     * UserGroups constructor.
     * @param $users
     */
    public function __construct($users)
    {
        $this->users = $users;
    }

    public function getUsersByRole(string $role) : array
    {
        $result = [];
        foreach ($this->users as $user){
            if ($user->getRoles() === [$role])
                array_push($result, $user);
        }
        return $result;
    }

}