<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 13.05.2018
 * Time: 17:37
 */

namespace App\Util;
use Pagerfanta\Exception\LogicException;
use Pagerfanta\Pagerfanta;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
class EntityToJSON
{
    private $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function serializePagerfanta(array $excludedFields = [])
    {
        if ($this->data instanceof Pagerfanta){
            $pageData = PagerData::getPageData($this->data);
            $formattedData = array(
                'data' => $this->data,
                'params' => [
                    'pagination' => $pageData,
                ]
            );
            return $this->serialize($formattedData, $excludedFields);
        }
        else throw new LogicException();
    }

    public function serializeEntity(array $excludedFields = [])
    {
        $formattedData = array(
            'data' => $this->data,
            'params' => []
        );
        return $this->serialize($formattedData, $excludedFields);
    }
    private function serialize(array $formattedData, array $excludedFields = [])
    {
        $normalizer = new ObjectNormalizer();
        $normalizer->setIgnoredAttributes($excludedFields);
        $encoder = new JsonEncoder();
        $serializer = new Serializer(array($normalizer), array($encoder));
        return $serializer->serialize($formattedData, 'json');
    }

}