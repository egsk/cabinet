<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 07.05.2018
 * Time: 23:28
 */

namespace App\Util;


class RecursiveFolderCreation
{
    public static function createFolders(string $path, string $fileName = 'voidFileName.void'): void
    {
        $clearPath = str_replace(DIRECTORY_SEPARATOR . $fileName, '', $path);
        $folders = explode(DIRECTORY_SEPARATOR, $clearPath);
        $currentFolder = '';
        for ($i = 0; $i < count($folders); $i++){
            if ($folders[$i] === '..'){
                $j = $i + 1;
                $downCount = 1;
                while ($folders[$j] === '..' ){
                    $downCount++;
                    $j++;
                }
                $folders = array_diff($folders, array_slice($folders, $i-$downCount, $downCount*2));
                $i++;
            }
        }
        foreach ($folders as $folder) {
            $currentFolder .= $folder . DIRECTORY_SEPARATOR;
            if (!file_exists($currentFolder)) {
                mkdir($currentFolder, 0755);
            }
        }
    }
}