    /**
     * @return Collection|Link[]
     */
    public function getCorrection(): Collection
    {
        return $this->correction;
    }

    public function addCorrection(Link $correction): self
    {
        if (!$this->correction->contains($correction)) {
            $this->correction[] = $correction;
            $correction->setCorrection($this);
        }

        return $this;
    }

    public function removeCorrection(Link $correction): self
    {
        if ($this->correction->contains($correction)) {
            $this->correction->removeElement($correction);
            // set the owning side to null (unless already changed)
            if ($correction->getCorrection() === $this) {
                $correction->setCorrection(null);
            }
        }

        return $this;
    }