<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LinkRepository")
 */
class Link
{

    const PATH =
        __DIR__ . DIRECTORY_SEPARATOR . '..' .
        DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .
        DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .
        DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR .
        'data' . DIRECTORY_SEPARATOR;



    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $textLink;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Message", inversedBy="links", cascade={"persist"})
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="isd1", cascade={"persist"})
     */
    private $isd1;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp2", cascade={"persist"})
     */
    private $oksp2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp3", cascade={"persist"})
     */
    private $oksp3;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp4", cascade={"persist"})
     */
    private $oksp4;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp5", cascade={"persist"})
     */
    private $oksp5;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp6", cascade={"persist"})
     */
    private $oksp6;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp7", cascade={"persist"})
     */
    private $oksp7;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp8", cascade={"persist"})
     */
    private $oksp8;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp9", cascade={"persist"})
     */
    private $oksp9;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="locs10", cascade={"persist"})
     */
    private $locs10;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp101", cascade={"persist"})
     */
    private $oksp101;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp11", cascade={"persist"})
     */
    private $oksp11;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="oksp12", cascade={"persist"})
     */
    private $oksp12;
    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="correction", cascade={"persist"})
     */
    private $requestCorrection;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="links")
     * @ORM\JoinColumn(nullable=false)
     */
    private $owner;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="contract", cascade={"persist"})
     */
    private $contract;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="adminContract", cascade={"persist"})
     */
    private $adminContract;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="bill", cascade={"persist"})
     */
    private $bill;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="decision", cascade={"persist"})
     */
    private $decision;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UserRequest", inversedBy="remark", cascade={"persist"})
     */
    private $remark;


    public function getId()
    {
        return $this->id;
    }

    public function getTextLink(): ?string
    {
        return $this->textLink;
    }

    public function setTextLink(string $textLink): self
    {
        $this->textLink = $textLink;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getMessage(): ?Message
    {
        return $this->message;
    }

    public function setMessage(?Message $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getIsd1(): ?UserRequest
    {
        return $this->isd1;
    }

    public function setIsd1(?UserRequest $isd1): self
    {
        $this->isd1 = $isd1;

        return $this;
    }

    public function getOksp2(): ?UserRequest
    {
        return $this->oksp2;
    }

    public function setOksp2(?UserRequest $oksp2): self
    {
        $this->oksp2 = $oksp2;

        return $this;
    }

    public function getOksp3(): ?UserRequest
    {
        return $this->oksp3;
    }

    public function setOksp3(?UserRequest $oksp3): self
    {
        $this->oksp3 = $oksp3;

        return $this;
    }

    public function getOksp4(): ?UserRequest
    {
        return $this->oksp4;
    }

    public function setOksp4(?UserRequest $oksp4): self
    {
        $this->oksp4 = $oksp4;

        return $this;
    }

    public function getOksp5(): ?UserRequest
    {
        return $this->oksp5;
    }

    public function setOksp5(?UserRequest $oksp5): self
    {
        $this->oksp5 = $oksp5;

        return $this;
    }

    public function getOksp6(): ?UserRequest
    {
        return $this->oksp6;
    }

    public function setOksp6(?UserRequest $oksp6): self
    {
        $this->oksp6 = $oksp6;

        return $this;
    }

    public function getOksp7(): ?UserRequest
    {
        return $this->oksp7;
    }

    public function setOksp7(?UserRequest $oksp7): self
    {
        $this->oksp7 = $oksp7;

        return $this;
    }

    public function getOksp8(): ?UserRequest
    {
        return $this->oksp8;
    }

    public function setOksp8(?UserRequest $oksp8): self
    {
        $this->oksp8 = $oksp8;

        return $this;
    }

    public function getOksp9(): ?UserRequest
    {
        return $this->oksp9;
    }

    public function setOksp9(?UserRequest $oksp9): self
    {
        $this->oksp9 = $oksp9;

        return $this;
    }

    public function getLocs10(): ?UserRequest
    {
        return $this->locs10;
    }

    public function setLocs10(?UserRequest $locs10): self
    {
        $this->locs10 = $locs10;

        return $this;
    }

    public function getOksp101(): ?UserRequest
    {
        return $this->oksp101;
    }

    public function setOksp101(?UserRequest $oksp101): self
    {
        $this->oksp101 = $oksp101;

        return $this;
    }

    public function getOksp11(): ?UserRequest
    {
        return $this->oksp11;
    }

    public function setOksp11(?UserRequest $oksp11): self
    {
        $this->oksp11 = $oksp11;

        return $this;
    }

    public function getOksp12(): ?UserRequest
    {
        return $this->oksp11;
    }

    public function setOksp12(?UserRequest $oksp12): self
    {
        $this->oksp12 = $oksp12;

        return $this;
    }

    public function getRequestCorrection(): ?UserRequest
    {
        return $this->requestCorrection;
    }

    public function setRequestCorrection(?UserRequest $correction): self
    {
        $this->requestCorrection = $correction;

        return $this;
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(?User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getContract(): ?UserRequest
    {
        return $this->contract;
    }

    public function setContract(?UserRequest $contract): self
    {
        $this->contract = $contract;

        return $this;
    }

    public function getAdminContract(): ?UserRequest
    {
        return $this->adminContract;
    }

    public function setAdminContract(?UserRequest $adminContracts): self
    {
        $this->adminContract = $adminContracts;

        return $this;
    }

    public function getBill(): ?UserRequest
    {
        return $this->bill;
    }

    public function setBill(?UserRequest $bills): self
    {
        $this->bill = $bills;

        return $this;
    }

    public function getDecision(): ?UserRequest
    {
        return $this->decision;
    }

    public function setDecision(?UserRequest $decisions): self
    {
        $this->decision = $decisions;

        return $this;
    }

    public function getRemark(): ?UserRequest
    {
        return $this->remark;
    }

    public function setRemark(?UserRequest $remarks): self
    {
        $this->remark = $remarks;

        return $this;
    }

}
