<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Validator\Constraints as AcmeAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRequestRepository")
 */
class UserRequest
{
    const allowedExtensions = [
        'doc', 'docx', 'odt',
        'pdf',
        'xls', 'xlsx', 'ods'
    ];

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $objectName;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="userRequests")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean")
     */
    private $published;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="contract", cascade={"persist", "remove"})
     */
    private $contract;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="isd1", cascade={"persist", "remove"})
     */
    private $isd1;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp2", cascade={"persist", "remove"})
     */
    private $oksp2;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp3", cascade={"persist", "remove"})
     */
    private $oksp3;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp4", cascade={"persist", "remove"})
     */
    private $oksp4;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp5", cascade={"persist", "remove"})
     */
    private $oksp5;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp6", cascade={"persist", "remove"})
     */
    private $oksp6;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp7", cascade={"persist", "remove"})
     */
    private $oksp7;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp8", cascade={"persist", "remove"})
     */
    private $oksp8;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp9", cascade={"persist"})
     */
    private $oksp9;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="locs10", cascade={"persist", "remove"})
     */
    private $locs10;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp101", cascade={"persist", "remove"})
     */
    private $oksp101;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp11", cascade={"persist", "remove"})
     */
    private $oksp11;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="requestCorrection", cascade={"persist", "remove"})
     */
    private $correction;
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="oksp12", cascade={"persist", "remove"})
     */
    private $oksp12;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Status", inversedBy="userRequests")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="adminContract", cascade={"persist", "remove"})
     */
    private $adminContract;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="bill", cascade={"persist", "remove"})
     */
    private $bill;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="decision", cascade={"persist", "remove"})
     */
    private $decision;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="remark", cascade={"persist", "remove"})
     */
    private $remark;

    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $contractFiles;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $isd1Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp2Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp3Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp4Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp5Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp6Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp7Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp8Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp9Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $locs10Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp101Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp11Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $oksp12Files;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $correctionFiles;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $adminContractFiles;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $billFiles;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $decisionFiles;
    /**
     * @AcmeAssert\IsFilesAreAllowed()
     */
    private $remarkFiles;
    private $sendEmail = true;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="userRequest", cascade={"remove"})
     */
    private $messages;


    public function __construct()
    {
        $this->isd1 = new ArrayCollection();
        $this->oksp2 = new ArrayCollection();
        $this->oksp3 = new ArrayCollection();
        $this->oksp4 = new ArrayCollection();
        $this->oksp5 = new ArrayCollection();
        $this->oksp6 = new ArrayCollection();
        $this->oksp7 = new ArrayCollection();
        $this->oksp8 = new ArrayCollection();
        $this->oksp9 = new ArrayCollection();
        $this->locs10 = new ArrayCollection();
        $this->oksp101 = new ArrayCollection();
        $this->oksp11 = new ArrayCollection();
        $this->oksp12 = new ArrayCollection();
        $this->correction = new ArrayCollection();
        $this->contract = new ArrayCollection();
        $this->adminContract = new ArrayCollection();
        $this->bill = new ArrayCollection();
        $this->decision = new ArrayCollection();
        $this->remark = new ArrayCollection();
        $this->messages = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getObjectName(): ?string
    {
        return $this->objectName;
    }

    public function setObjectName(string $objectName): self
    {
        $this->objectName = $objectName;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPublished(): ?bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getIsd1(): Collection
    {
        return $this->isd1;
    }

    public function addIsd1(Link $isd1): self
    {
        if (!$this->isd1->contains($isd1)) {
            $this->isd1[] = $isd1;
            $isd1->setIsd1($this);
        }

        return $this;
    }

    public function removeIsd1(Link $isd1): self
    {
        if ($this->isd1->contains($isd1)) {
            $this->isd1->removeElement($isd1);
            // set the owning side to null (unless already changed)
            if ($isd1->getIsd1() === $this) {
                $isd1->setIsd1(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getOksp2(): Collection
    {
        return $this->oksp2;
    }

    public function addOksp2(Link $oksp2): self
    {
        if (!$this->oksp2->contains($oksp2)) {
            $this->oksp2[] = $oksp2;
            $oksp2->setOksp2($this);
        }

        return $this;
    }

    public function removeOksp2(Link $oksp2): self
    {
        if ($this->oksp2->contains($oksp2)) {
            $this->oksp2->removeElement($oksp2);
            // set the owning side to null (unless already changed)
            if ($oksp2->getOksp2() === $this) {
                $oksp2->setOksp2(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getOksp3(): Collection
    {
        return $this->oksp3;
    }

    public function addOksp3(Link $oksp3): self
    {
        if (!$this->oksp3->contains($oksp3)) {
            $this->oksp3[] = $oksp3;
            $oksp3->setOksp3($this);
        }

        return $this;
    }

    public function removeOksp3(Link $oksp3): self
    {
        if ($this->oksp3->contains($oksp3)) {
            $this->oksp3->removeElement($oksp3);
            // set the owning side to null (unless already changed)
            if ($oksp3->getOksp3() === $this) {
                $oksp3->setOksp3(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getOksp4(): Collection
    {
        return $this->oksp4;
    }

    public function addOksp4(Link $oksp4): self
    {
        if (!$this->oksp4->contains($oksp4)) {
            $this->oksp4[] = $oksp4;
            $oksp4->setOksp4($this);
        }

        return $this;
    }

    public function removeOksp4(Link $oksp4): self
    {
        if ($this->oksp4->contains($oksp4)) {
            $this->oksp4->removeElement($oksp4);
            // set the owning side to null (unless already changed)
            if ($oksp4->getOksp4() === $this) {
                $oksp4->setOksp4(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Link[]
     */
    public function getOksp5(): Collection
    {
        return $this->oksp5;
    }

    public function addOksp5(Link $oksp5): self
    {
        if (!$this->oksp5->contains($oksp5)) {
            $this->oksp5[] = $oksp5;
            $oksp5->setOksp5($this);
        }

        return $this;
    }

    public function removeOksp5(Link $oksp5): self
    {
        if ($this->oksp5->contains($oksp5)) {
            $this->oksp5->removeElement($oksp5);
            // set the owning side to null (unless already changed)
            if ($oksp5->getOksp5() === $this) {
                $oksp5->setOksp5(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Link[]
     */
    public function getOksp6(): Collection
    {
        return $this->oksp6;
    }

    public function addOksp6(Link $oksp6): self
    {
        if (!$this->oksp6->contains($oksp6)) {
            $this->oksp6[] = $oksp6;
            $oksp6->setOksp6($this);
        }

        return $this;
    }

    public function removeOksp6(Link $oksp6): self
    {
        if ($this->oksp6->contains($oksp6)) {
            $this->oksp6->removeElement($oksp6);
            // set the owning side to null (unless already changed)
            if ($oksp6->getOksp6() === $this) {
                $oksp6->setOksp6(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Link[]
     */
    public function getOksp7(): Collection
    {
        return $this->oksp7;
    }

    public function addOksp7(Link $oksp7): self
    {
        if (!$this->oksp7->contains($oksp7)) {
            $this->oksp7[] = $oksp7;
            $oksp7->setOksp7($this);
        }

        return $this;
    }

    public function removeOksp7(Link $oksp7): self
    {
        if ($this->oksp7->contains($oksp7)) {
            $this->oksp7->removeElement($oksp7);
            // set the owning side to null (unless already changed)
            if ($oksp7->getOksp7() === $this) {
                $oksp7->setOksp7(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Link[]
     */
    public function getOksp8(): Collection
    {
        return $this->oksp8;
    }

    public function addOksp8(Link $oksp8): self
    {
        if (!$this->oksp8->contains($oksp8)) {
            $this->oksp8[] = $oksp8;
            $oksp8->setOksp8($this);
        }

        return $this;
    }

    public function removeOksp8(Link $oksp8): self
    {
        if ($this->oksp8->contains($oksp8)) {
            $this->oksp8->removeElement($oksp8);
            // set the owning side to null (unless already changed)
            if ($oksp8->getOksp8() === $this) {
                $oksp8->setOksp8(null);
            }
        }

        return $this;
    }


    /**
     * @return Collection|Link[]
     */
    public function getOksp9(): Collection
    {
        return $this->oksp9;
    }

    public function addOksp9(Link $oksp9): self
    {
        if (!$this->oksp9->contains($oksp9)) {
            $this->oksp9[] = $oksp9;
            $oksp9->setOksp9($this);
        }

        return $this;
    }

    public function removeOksp9(Link $oksp9): self
    {
        if ($this->oksp9->contains($oksp9)) {
            $this->oksp9->removeElement($oksp9);
            // set the owning side to null (unless already changed)
            if ($oksp9->getOksp9() === $this) {
                $oksp9->setOksp9(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getLocs10(): Collection
    {
        return $this->locs10;
    }

    public function addLocs10(Link $locs10): self
    {
        if (!$this->locs10->contains($locs10)) {
            $this->locs10[] = $locs10;
            $locs10->setLocs10($this);
        }

        return $this;
    }

    public function removeLocs10(Link $locs10): self
    {
        if ($this->locs10->contains($locs10)) {
            $this->locs10->removeElement($locs10);
            // set the owning side to null (unless already changed)
            if ($locs10->getLocs10() === $this) {
                $locs10->setLocs10(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getOksp101(): Collection
    {
        return $this->oksp101;
    }

    public function addOksp101(Link $oksp101): self
    {
        if (!$this->oksp101->contains($oksp101)) {
            $this->oksp101[] = $oksp101;
            $oksp101->setOksp101($this);
        }

        return $this;
    }

    public function removeOksp101(Link $oksp101): self
    {
        if ($this->oksp101->contains($oksp101)) {
            $this->oksp101->removeElement($oksp101);
            // set the owning side to null (unless already changed)
            if ($oksp101->getOksp101() === $this) {
                $oksp101->setOksp101(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getOksp11(): Collection
    {
        return $this->oksp11;
    }

    public function addOksp11(Link $oksp11): self
    {
        if (!$this->oksp11->contains($oksp11)) {
            $this->oksp11[] = $oksp11;
            $oksp11->setOksp11($this);
        }

        return $this;
    }

    public function removeOksp11(Link $oksp11): self
    {
        if ($this->oksp11->contains($oksp11)) {
            $this->oksp11->removeElement($oksp11);
            // set the owning side to null (unless already changed)
            if ($oksp11->getOksp11() === $this) {
                $oksp11->setOksp11(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getOksp12(): Collection
    {
        return $this->oksp12;
    }

    public function addOksp12(Link $oksp12): self
    {
        if (!$this->oksp12->contains($oksp12)) {
            $this->oksp11[] = $oksp12;
            $oksp12->setOksp12($this);
        }

        return $this;
    }

    public function removeOksp12(Link $oksp12): self
    {
        if ($this->oksp11->contains($oksp12)) {
            $this->oksp11->removeElement($oksp12);
            // set the owning side to null (unless already changed)
            if ($oksp12->getOksp12() === $this) {
                $oksp12->setOksp12(null);
            }
        }

        return $this;
    }







    /**
     * @return Collection|Link[]
     */
    public function getCorrection(): Collection
    {
        return $this->correction;
    }

    public function addCorrection(Link $correction): self
    {
        if (!$this->correction->contains($correction)) {
            $this->correction[] = $correction;
            $correction->setRequestCorrection($this);
        }

        return $this;
    }

    public function removeCorrection(Link $correction): self
    {
        if ($this->correction->contains($correction)) {
            $this->correction->removeElement($correction);
            // set the owning side to null (unless already changed)
            if ($correction->getRequestCorrection() === $this) {
                $correction->setRequestCorrection(null);
            }
        }

        return $this;
    }

    public function getContractFiles()
    {
        return $this->contractFiles;
    }

    public function setContractFiles($files)
    {
        $this->contractFiles = $files;
    }

    public function getIsd1Files()
    {
        return $this->isd1Files;
    }

    public function setIsd1Files($files)
    {
        $this->isd1Files = $files;
    }

    public function getOksp2Files()
    {
        return $this->oksp2Files;
    }

    public function setOksp2Files($files)
    {
        $this->oksp2Files = $files;
    }

    public function getOksp3Files()
    {
        return $this->oksp3Files;
    }

    public function setOksp3Files($files)
    {
        $this->oksp3Files = $files;
    }

    public function getOksp4Files()
    {
        return $this->oksp4Files;
    }

    public function setOksp4Files($files)
    {
        $this->oksp4Files = $files;
    }

    public function getOksp5Files()
    {
        return $this->oksp5Files;
    }

    public function setOksp5Files($files)
    {
        $this->oksp5Files = $files;
    }

    public function getOksp6Files()
    {
        return $this->oksp6Files;
    }

    public function setOksp6Files($files)
    {
        $this->oksp6Files = $files;
    }


    public function getOksp7Files()
    {
        return $this->oksp7Files;
    }

    public function setOksp7Files($files)
    {
        $this->oksp7Files = $files;
    }

    public function getOksp8Files()
    {
        return $this->oksp8Files;
    }

    public function setOksp8Files($files)
    {
        $this->oksp8Files = $files;
    }

    public function getOksp9Files()
    {
        return $this->oksp9Files;
    }

    public function setOksp9Files($files)
    {
        $this->oksp9Files = $files;
    }

    public function getLocs10Files()
    {
        return $this->locs10Files;
    }

    public function setLocs10Files($files)
    {
        $this->locs10Files = $files;
    }

    public function getOksp101Files()
    {
        return $this->oksp101Files;
    }

    public function setOksp101Files($files)
    {
        $this->oksp101Files = $files;
    }

    public function getOksp11Files()
    {
        return $this->oksp11Files;
    }

    public function setOksp11Files($files)
    {
        $this->oksp11Files = $files;
    }

    public function getOksp12Files()
    {
        return $this->oksp12Files;
    }

    public function setOksp12Files($files)
    {
        $this->oksp12Files = $files;
    }


    public function getCorrectionFiles()
    {
        return $this->correctionFiles;
    }

    public function setCorrectionFiles($files)
    {
        $this->correctionFiles = $files;
    }


    public function getAdminContractFiles()
    {
        return $this->adminContractFiles;
    }

    public function setAdminContractFiles($files)
    {
        $this->adminContractFiles = $files;
    }

    public function getBillFiles()
    {
        return $this->billFiles;
    }

    public function setBillFiles($files)
    {
        $this->billFiles = $files;
    }

    public function getDecisionFiles()
    {
        return $this->decisionFiles;
    }

    public function setDecisionFiles($files)
    {
        $this->decisionFiles = $files;
    }

    public function getRemarkFiles()
    {
        return $this->remarkFiles;
    }

    public function setRemarkFiles($files)
    {
        $this->remarkFiles = $files;
    }

    public function getStatus(): ?Status
    {
        return $this->status;
    }

    public function setStatus(?Status $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getContract(): Collection
    {
        return $this->contract;
    }

    public function addContract(Link $contract): self
    {
        if (!$this->contract->contains($contract)) {
            $this->contract[] = $contract;
            $contract->setContract($this);
        }

        return $this;
    }

    public function removeContract(Link $contract): self
    {
        if ($this->contract->contains($contract)) {
            $this->contract->removeElement($contract);
            // set the owning side to null (unless already changed)
            if ($contract->getContract() === $this) {
                $contract->setContract(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getAdminContract(): Collection
    {
        return $this->adminContract;
    }

    public function addAdminContract(Link $adminContract): self
    {
        if (!$this->adminContract->contains($adminContract)) {
            $this->adminContract[] = $adminContract;
            $adminContract->setAdminContract($this);
        }

        return $this;
    }

    public function removeAdminContract(Link $adminContract): self
    {
        if ($this->adminContract->contains($adminContract)) {
            $this->adminContract->removeElement($adminContract);
            // set the owning side to null (unless already changed)
            if ($adminContract->getAdminContract() === $this) {
                $adminContract->setAdminContract(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getBill(): Collection
    {
        return $this->bill;
    }

    public function addBill(Link $bill): self
    {
        if (!$this->bill->contains($bill)) {
            $this->bill[] = $bill;
            $bill->setBill($this);
        }

        return $this;
    }

    public function removeBill(Link $bill): self
    {
        if ($this->bill->contains($bill)) {
            $this->bill->removeElement($bill);
            // set the owning side to null (unless already changed)
            if ($bill->getBill() === $this) {
                $bill->setBill(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getDecision(): Collection
    {
        return $this->decision;
    }

    public function addDecision(Link $decision): self
    {
        if (!$this->decision->contains($decision)) {
            $this->decision[] = $decision;
            $decision->setDecision($this);
        }

        return $this;
    }

    public function removeDecision(Link $decision): self
    {
        if ($this->decision->contains($decision)) {
            $this->decision->removeElement($decision);
            // set the owning side to null (unless already changed)
            if ($decision->getDecision() === $this) {
                $decision->setDecision(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getRemark(): Collection
    {
        return $this->remark;
    }

    public function addRemark(Link $remark): self
    {
        if (!$this->remark->contains($remark)) {
            $this->remark[] = $remark;
            $remark->setRemark($this);
        }

        return $this;
    }

    public function removeRemark(Link $remark): self
    {
        if ($this->remark->contains($remark)) {
            $this->remark->removeElement($remark);
            // set the owning side to null (unless already changed)
            if ($remark->getRemark() === $this) {
                $remark->setRemark(null);
            }
        }

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function setSendEmail(bool $send)
    {
        $this->sendEmail = $send;

        return $this;
    }

    public function getSendEmail(): bool
    {
        return $this->sendEmail;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setUserRequest($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getUserRequest() === $this) {
                $message->setUserRequest(null);
            }
        }

        return $this;
    }
}
