<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Util\HideEmail;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use App\Validator\Constraints as AcmeAssert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user_list")
 * @UniqueEntity(
 *     fields={"email", "phone"}
 * )
 */
class User implements UserInterface, \Serializable
{

    private const roleHierarchy =
        [
            'ROLE_ADMIN' => 'Администратор',
            'ROLE_USER' => 'Read-only',
            'ROLE_INACTIVE' => 'Неподтвержденный пользователь'
        ];
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string")
     * @Assert\Length(
     *     max=100,
     *     min=3,
     *     minMessage="Указано слишком короткое имя",
     *     maxMessage="Указано слишком длинное имя")
     */
    private $fullName;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     * @Assert\Email(
     *     message = "Неправильно указан E-mail")
     */
    private $email;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @var string
     * @ORM\Column(type="string", unique=true)
     * @AcmeAssert\IsPhone
     */
    private $phone;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $company;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var array
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    private $confirmToken;

    /**
     * @var string
     * @Assert\Blank()
     */
    private $addressCustom;

    private $plainPassword;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Dialog", mappedBy="user", cascade={"persist", "remove"})
     */
    private $dialog;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="owner")
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserRequest", mappedBy="user")
     */
    private $userRequests;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Link", mappedBy="owner")
     */
    private $links;


    public function __construct()
    {
        $this->roles[] = 'ROLE_INACTIVE';
        $this->messages = new ArrayCollection();
        $this->userRequests = new ArrayCollection();
        $this->links = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function setFullName(string $fullName): self
    {
        $this->fullName = $fullName;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function getPrivateEmail(): ?string
    {
        return HideEmail::hide($this->email);
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    public function setPlainPassword(string $plainPassword): self
    {
        $this->plainPassword = $plainPassword;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getConfirmToken(): ?string
    {
        return $this->confirmToken;
    }

    public function setConfirmToken($confirmToken): self
    {
        $this->confirmToken = $confirmToken;

        return $this;
    }

    public function getCreatedAt(): ?\DateTime
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getRoles(): array
    {
        $roles = $this->roles;

        if (empty($roles)) {
            $roles[] = 'ROLE_INACTIVE';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function getSalt(): ?string
    {
        return null;
    }

    public function eraseCredentials(): void
    {
        $this->plainPassword = null;
    }

    public function getHighestRoleText(): ?string
    {
        foreach ($this->roles as $role)
        {
            if (in_array($role, $this->roles))
            {
                return self::roleHierarchy[$role];
            }
        }
        return null;
    }
    public function getHighestRole(): ?string
    {
        foreach ($this->roles as $role)
        {
            if (in_array($role, $this->roles))
            {
                return $role;
            }
        }
        return null;
    }

    public function serialize(): string
    {
        return serialize([$this->id, $this->email, $this->password]);
    }

    public function unserialize($serialized): void
    {
        [$this->id, $this->email, $this->password] = unserialize($serialized, ['allowed_classes' => false]);
    }

    public function getDialog(): ?Dialog
    {
        return $this->dialog;
    }

    public function setDialog(?Dialog $dialog): self
    {
        $this->dialog = $dialog;

        // set (or unset) the owning side of the relation if necessary
        $newUser = $dialog === null ? null : $this;
        if ($newUser !== $dialog->getUser()) {
            $dialog->setUser($newUser);
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setOwner($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getOwner() === $this) {
                $message->setOwner(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserRequest[]
     */
    public function getUserRequests(): Collection
    {
        return $this->userRequests;
    }

    public function addUserRequest(UserRequest $userRequest): self
    {
        if (!$this->userRequests->contains($userRequest)) {
            $this->userRequests[] = $userRequest;
            $userRequest->setUser($this);
        }

        return $this;
    }

    public function removeUserRequest(UserRequest $userRequest): self
    {
        if ($this->userRequests->contains($userRequest)) {
            $this->userRequests->removeElement($userRequest);
            // set the owning side to null (unless already changed)
            if ($userRequest->getUser() === $this) {
                $userRequest->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->setOwner($this);
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        if ($this->links->contains($link)) {
            $this->links->removeElement($link);
            // set the owning side to null (unless already changed)
            if ($link->getOwner() === $this) {
                $link->setOwner(null);
            }
        }

        return $this;
    }

    public function getAddressCustom(): ?string
    {
        return $this->addressCustom;
    }

    public function setAddressCustom(string $address): self
    {
        $this->addressCustom = $address;

        return $this;
    }

}
