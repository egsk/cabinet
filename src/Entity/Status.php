<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StatusRepository")
 */
class Status
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true, length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $content;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Dialog", mappedBy="status")
     */
    private $dialogs;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Message", mappedBy="status")
     */
    private $messages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\UserRequest", mappedBy="status")
     */
    private $userRequests;

    public function __construct()
    {
        $this->dialogs = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->userRequests = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return Collection|Dialog[]
     */
    public function getDialogs(): Collection
    {
        return $this->dialogs;
    }

    public function addDialog(Dialog $dialog): self
    {
        if (!$this->dialogs->contains($dialog)) {
            $this->dialogs[] = $dialog;
            $dialog->setStatus($this);
        }

        return $this;
    }

    public function removeDialog(Dialog $dialog): self
    {
        if ($this->dialogs->contains($dialog)) {
            $this->dialogs->removeElement($dialog);
            // set the owning side to null (unless already changed)
            if ($dialog->getStatus() === $this) {
                $dialog->setStatus(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Message[]
     */
    public function getMessages(): Collection
    {
        return $this->messages;
    }

    public function addMessage(Message $message): self
    {
        if (!$this->messages->contains($message)) {
            $this->messages[] = $message;
            $message->setStatus($this);
        }

        return $this;
    }

    public function removeMessage(Message $message): self
    {
        if ($this->messages->contains($message)) {
            $this->messages->removeElement($message);
            // set the owning side to null (unless already changed)
            if ($message->getStatus() === $this) {
                $message->setStatus(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserRequest[]
     */
    public function getUserRequests(): Collection
    {
        return $this->userRequests;
    }

    public function addUserRequest(UserRequest $userRequest): self
    {
        if (!$this->userRequests->contains($userRequest)) {
            $this->userRequests[] = $userRequest;
            $userRequest->setStatus($this);
        }

        return $this;
    }

    public function removeUserRequest(UserRequest $userRequest): self
    {
        if ($this->userRequests->contains($userRequest)) {
            $this->userRequests->removeElement($userRequest);
            // set the owning side to null (unless already changed)
            if ($userRequest->getStatus() === $this) {
                $userRequest->setStatus(null);
            }
        }

        return $this;
    }
}
