<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 19.06.2018
 * Time: 13:28
 */

namespace App\DataFixtures;

use App\Entity\Status;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture{

    public function load(ObjectManager $manager)
    {

        $user1 = new User();
        $user1->setEmail('info@ekspertiza-es.ru');
        $user1->setFullName('Администратор');
        $user1->setPlainPassword('admin');
        $user1->setPhone('+7 (8172) 78-70-50');
        $user1->setCompany('ООО «ЭнергоЭкспертСтрой»');
        $user1->setRoles(['ROLE_ADMIN']);

        $manager->persist($user1);

        $user2 = new User();
        $user2->setEmail('user@user.ru');
        $user2->setFullName('Полный пользователь');
        $user2->setPlainPassword('user');
        $user2->setPhone('+79212222222');
        $user2->setCompany('Компания2');
        $user2->setRoles(['ROLE_USER']);

        $manager->persist($user2);

        $user3 = new User();
        $user3->setEmail('user_inactive@user.ru');
        $user3->setFullName('Неподтвержденный пользователь');
        $user3->setPlainPassword('user');
        $user3->setRoles(['ROLE_INACTIVE']);
        $user3->setPhone('+79213333333');
        $user3->setCompany('Компания3');

        $manager->persist($user3);

        $user4 = new User();
        $user4->setEmail('user_banned@user.ru');
        $user4->setFullName('Заблокированный пользователь');
        $user4->setPlainPassword('user');
        $user4->setRoles(['ROLE_DENIED']);
        $user4->setPhone('+79214444444');
        $user4->setCompany('Компания4');

        $manager->persist($user4);


        $status1 = new Status();
        $status1->setName('new');
        $status1->setContent('Получено в работу');

        $manager->persist($status1);

        $status2 = new Status();
        $status2->setName('watched');
        $status2->setContent('Ведется работа');

        $manager->persist($status2);


        $status3 = new Status();
        $status3->setName('finished');
        $status3->setContent('Закончено');

        $manager->persist($status3);

        $status4 = new Status();
        $status4->setName('declined');
        $status4->setContent('Отклонено');

        $manager->persist($status4);

        $status5 = new Status();
        $status5->setName('refreshed');
        $status5->setContent('Обновлено');

        $manager->persist($status5);

        $status6 = new Status();
        $status6->setName('active');
        $status6->setContent('Активно');

        $manager->persist($status6);

        $status7 = new Status();
        $status7->setName('draft');
        $status7->setContent('Черновик');

        $manager->persist($status7);

        $manager->flush();

    }

}