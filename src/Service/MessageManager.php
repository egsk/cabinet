<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 24.06.2018
 * Time: 21:27
 */

namespace App\Service;
use App\Entity\Message;
use Doctrine\ORM\EntityManager;

class MessageManager
{
    private $entityManager;


    public function __construct(EntityManager $manager)
    {
        $this->entityManager = $manager;
    }

    public function deleteMessage(Message $message)
    {
        $this->entityManager->remove($message);
        $this->entityManager->flush();
    }
}