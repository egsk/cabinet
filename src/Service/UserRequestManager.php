<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 01.07.2018
 * Time: 22:26
 */

namespace App\Service;


use App\Entity\UserRequest;
use App\Entity\User;
use Doctrine\ORM\EntityManager;

class UserRequestManager
{

    private const UserRequestSubdirs = [
        'contract'      => 'Заявление, приложения',
        'isd1'          => 'Раздел1',
        'oksp2'         => 'Раздел2',
        'oksp3'         => 'Раздел3',
        'oksp4'         => 'Раздел4',
        'oksp5'         => 'Раздел5',
        'oksp6'         => 'Раздел6',
        'oksp7'         => 'Раздел7',
        'oksp8'         => 'Раздел8',
        'oksp9'         => 'Раздел9',
        'locs10'        => 'Раздел0',
        'oksp101'       => 'Раздел10_1',
        'oksp11'        => 'Раздел11',
        'oksp12'        => 'Раздел12',
        'correction'    => 'Ответы на правки',
        'adminContract' => 'Договор',
        'bill'          => 'Счет',
        'remark'        => 'Замечания',
        'decision'      => 'Заключение',
    ];

    /**
     * @var User
     */
    private $user;

    /**
     * @var UserRequest
     */
    private $userRequest;

    /**
     * @var EntityManager
     */
    private $entityManager;

    private $mailManager;

    private $isInit;


    public function __construct(EntityManager $entityManager, MailManager $manager)
    {
        $this->entityManager = $entityManager;
        $this->mailManager = $manager;
    }

    public function init(UserRequest $request, User $user)
    {
        $this->userRequest = $request;
        $this->user = $user;
        $this->isInit = true;
    }

    public function saveUserRequest(string $statusName = 'new', bool $sendMail = true)
    {
        date_default_timezone_set('Europe/Moscow');

        $statusRepository = $this->entityManager->getRepository('App:Status');

        if (!$this->userRequest->getPublished()) {
            $status = $statusRepository->getStatusByName('draft');
        } else {
            $status = $statusRepository->getStatusByName($statusName);
        }

        if (!$this->userRequest->getUser())
            $this->userRequest->setUser($this->user);

        if (!$this->userRequest->getCreatedAt()) {
            $createdAt = new \DateTime('now');
            $this->userRequest->setCreatedAt($createdAt);
        } else
            $createdAt = $this->userRequest->getCreatedAt();

        $fileDir = [$this->user->getEmail(), $createdAt->format('Ymdhis')];

        $this->userRequest->setStatus($status);


        foreach (self::UserRequestSubdirs as $method => $dir)
        {
            $method = ucfirst($method);
            $files = $this->userRequest->{'get' . $method . 'Files'}();
            if ($files){
                foreach ($files as $file){
                    $fileSaver = new FileUploader($file, $this->user, ['fileDir' => $fileDir]);
                    $link = $fileSaver->getLinkInstance(false, $dir);
                    $this->userRequest->{'add' . $method}($link);
                    $this->entityManager->persist($link);
                }
            }
        }

        $this->entityManager->persist($this->userRequest);



        $isNew = $this->userRequest->getId() ? false : true;

        $this->entityManager->flush();

        if ($isNew){
            $this->mailManager->nativeSendUserRequestMail($this->userRequest);
        } elseif ($this->userRequest->getSendEmail()) {
            $this->mailManager->nativeSendUserRequestEditedMail($this->userRequest);
        }
    }

    public function delete()
    {
        $this->entityManager->remove($this->userRequest);
        $this->entityManager->flush();
    }


}