<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 26.06.2018
 * Time: 21:37
 */

namespace App\Service;


use Doctrine\ORM\EntityManager;

class NavigationManager
{

    private $entityManager;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager =  $entityManager;
    }

    public function generateNavigationData($isAdmin = false)
    {
        $navigationData = [];

        $statusRepository = $this->entityManager->getRepository('App:Status');

        if ($isAdmin){
            $navigationData['type'] = 'admin';
            $navigationData['data'] = [
                'activeDialogsCount' => $statusRepository->getActiveDialogsCount(),
            ];
        }
    }


}