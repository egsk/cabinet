<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 21.06.2018
 * Time: 19:55
 */

namespace App\Service;


use App\Entity\Link;
use Doctrine\ORM\EntityManager;

class LinkManager
{

    /**
     * @var Link
     */
    private $link;

    /**
     * @var EntityManager
     */
    private $entityManager;

    private $isInit = false;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function init(Link $link)
    {
        $this->link = $link;
        $this->isInit = true;
    }

    public function getRealPath()
    {
        return Link::PATH . $this->link->getTextLink();
    }

    public function getRealName()
    {
        return $this->link->getFilename();
    }

    public function deleteLink()
    {
        $this->entityManager->remove($this->link);
        $this->entityManager->flush();
    }



}