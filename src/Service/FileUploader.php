<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 21.06.2018
 * Time: 21:44
 */

namespace App\Service;


use App\Entity\Link;
use App\Entity\User;
use App\Util\RecursiveFolderCreation;
use App\Util\Slugger;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var array
     */
    private $params;

    /**
     * @var User
     */
    private $owner;

    private $fileDir;

    private const defaultParams = [
      'rootDir' => Link::PATH,
      'fileDir' => 'default'
    ];

    public function __construct(UploadedFile $file, User $owner, array $params = [])
    {
        $this->file = $file;
        $this->owner = $owner;

        foreach (self::defaultParams as $defaultParamKey => $defaultParamValue)
        {
            if (!array_key_exists($defaultParamKey, $params))
                $params[$defaultParamKey] = $defaultParamValue;
        }
        $this->params = $params;

        $this->convertParams();
    }

    private function convertParams()
    {
        if (is_array($this->params['fileDir']))
            $this->params['fileDir'] = implode(DIRECTORY_SEPARATOR, $this->params['fileDir']);
    }

    public function saveFile($encodeFile = true, $subDir = '')
    {

        if ($encodeFile)
            $filename = sha1(uniqid(mt_rand(), true)) . '.' . $this->file->getClientOriginalExtension();
        else $filename = $this->file->getClientOriginalName();
        $this->fileDir = Slugger::strToUrl($this->params['fileDir']);
        if ($subDir) $this->fileDir .= DIRECTORY_SEPARATOR . $subDir . DIRECTORY_SEPARATOR;
        $path = $this->params['rootDir'] . $this->fileDir;
//        RecursiveFolderCreation::createFolders($path);

        if (file_exists($path . $filename)) unlink($path . $filename);

        $this->file->move($path, $filename);

        return $filename;
    }

    public function getLinkInstance($encodeFile = true, $subDir = '')
    {
        $link = new Link();

        $fileName = $this->saveFile($encodeFile, $subDir);
        $link->setTextLink($this->fileDir . $fileName);
        $link->setFilename($this->file->getClientOriginalName());
        $link->setOwner($this->owner);
        return $link;
    }


}