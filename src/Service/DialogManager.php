<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 24.06.2018
 * Time: 21:27
 */

namespace App\Service;
use App\Entity\Dialog;
use App\Entity\Message;
use App\Entity\User;
use Doctrine\ORM\EntityManager;

class DialogManager
{
    /**
     * @var Dialog
     */
    private $dialog;

    /**
     * @var Message
     */
    private $message;

    /**
     * @var User
     */
    private $user;

    private $isInit = false;

    private $entityManager;

    private $userRequestID;

    private $mailTo;

    /**
     * @var MailManager
     */
    private $mailManager;

    public function __construct(EntityManager $manager, MailManager $mailManager)
    {
        $this->entityManager = $manager;
        $this->mailManager = $mailManager;
    }

    public function init(Dialog $dialog,
                         Message $message = null,
                         User $user = null,
                         User $mailTo = null,
                         $userRequestID = null)
    {
        $this->dialog = $dialog;
        $this->message = $message;
        $this->user = $user;
        $this->userRequestID = $userRequestID;
        $this->mailTo = $mailTo;
        $this->isInit = true;
    }

    public function saveDialog()
    {
        if (!$this->isInit) return null;

        $this->saveMessage();
        $this->dialog->addMessage($this->message);
        if (!$this->dialog->getUser() && $this->mailTo) $this->dialog->setUser($this->mailTo);

        $this->mailManager->nativeSendMail($this->message);

        $this->entityManager->persist($this->dialog);
        $this->entityManager->flush();

        return $this->dialog;
    }

    private function saveMessage()
    {
        $statusRepository = $this->entityManager->getRepository('App:Status');
        $userRequestRepository = $this->entityManager->getRepository('App:UserRequest');
        $status = $statusRepository->getStatusByName('new');
        $this->message->setStatus($status);
        $this->message->setOwner($this->user);
        $files = $this->message->getFiles();
        $fileDir = $this->user->getEmail();

        if ($this->userRequestID){
            $userRequest = $userRequestRepository->find($this->userRequestID);
            if ($userRequest && ($this->dialog->getUser() === $userRequest->getUser()))
                $this->message->setUserRequest($userRequest);
        }


        if ($files){
            foreach ($files as $file){
                $fileSaver = new FileUploader($file, $this->user, ['fileDir' => $fileDir]);
                $link = $fileSaver->getLinkInstance();
                $this->message->addLink($link);
                $this->entityManager->persist($link);
            }
        }
        if ($this->user->getHighestRole() === 'ROLE_ADMIN')
            $this->message->setIsResponse(true);

        $this->entityManager->persist($this->message);

    }

    public function deleteDialog()
    {
        $this->entityManager->remove($this->dialog);
        $this->entityManager->flush();
    }

}