<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 02.07.2018
 * Time: 20:15
 */

namespace App\Service;


use App\Entity\Link;
use App\Entity\UserRequest;
use App\Entity\User;
use App\Util\Slugger;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ArchiveDownloader
{

    private $userRequest;

    private $filename = 'files';

    public function __construct(UserRequest $userRequest, $filename = 'files')
    {
        $this->userRequest = $userRequest;
        $this->filename = $filename;
    }


    public function createArchive()
    {
            $rootPath = realpath(
                Link::PATH .
                Slugger::strToUrl(
                    $this->userRequest->getUser()->getEmail() . '-' . $this->userRequest->getCreatedAt()->format('Ymdhis')));

            if (!$rootPath) throw new NotFoundHttpException('Не найдено файлов для создания архива.');
            $realPath = $rootPath . DIRECTORY_SEPARATOR . $this->filename . '.zip';

            $zip = new \ZipArchive();
            if(true !== $zip->open($realPath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE)) {
                return false;
            }
            $this->zipDir(
                rtrim($rootPath, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR,
                null,
                $zip
            );
            return $realPath;

    }

    private function zipDir($dir, $subdir, \ZipArchive $zip) {

        $files = scandir($dir.$subdir);

        foreach($files as $file) {

            if(in_array($file, array('.', '..')))
                continue;

            // check dir using real path
            if(is_dir($dir.$subdir.$file)) {

                // create folder using relative path
                $zip->addEmptyDir($subdir.$file);

                $this->zipDir(
                    $dir,                              // remember base dir
                    $subdir.$file.DIRECTORY_SEPARATOR, // relative path, don't forget separator
                    $zip                               // archive
                );
            }

            // file
            else {
                // get real path, set relative path
                $zip->addFile($dir.$subdir.$file, $subdir.$file);
            }
        }
    }


    public function createArchiveNoStructure()
    {
        $rootPath = realpath(
            Link::PATH .
            Slugger::strToUrl(
                $this->userRequest->getUser()->getEmail() . '-' . $this->userRequest->getCreatedAt()->format('Ymdhis')));

        $realPath = $rootPath . DIRECTORY_SEPARATOR . $this->filename . '.zip';

        $zip = new \ZipArchive();

        $zip->open($realPath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);

        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($rootPath),
            \RecursiveIteratorIterator::LEAVES_ONLY
        );

        foreach ($files as $name => $file)
        {
            if (!$file->isDir()){
                $filePath = $file->getRealPath();
                $relativePath = substr($filePath, strlen($realPath)+1);
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();

        return $realPath;
    }
}