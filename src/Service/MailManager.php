<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 20.06.2018
 * Time: 13:02
 */

namespace App\Service;



use App\Entity\Message;
use App\Entity\UserRequest;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
class MailManager
{
    private $mailer;
    private $templating;
    private $tokenStorage;
    public function __construct(\Twig_Environment $templating, \Swift_Mailer $mailer, TokenStorage $tokenStorage)
    {
        $this->mailer = $mailer;
        $this->templating = $templating;
        $this->tokenStorage = $tokenStorage;

    }


    public function nativeSendMail(Message $message)
    {
        $setFrom = getenv('ADMIN_EMAIL');
        $subject = "Новое сообщение";
        $setTo = $message->getIsResponse() ? $message->getDialog()->getUser()->getEmail() : $setFrom;
        $messageBody = $this->templating->render(
            'emails/newMessage.html.twig',
            [
                'message' => $message,
            ]
        );

        $headers = 'From: ' . $setFrom . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";

        mail($setTo, $subject, $messageBody, $headers);

    }

    public function nativeSendActivationMail(array $activationData)
    {
        $setFrom = getenv('ADMIN_EMAIL');
        $subject = "Активация аккаунта";
        $setTo = $activationData['userEmail'];
        $messageBody = $this->templating->render(
            'emails/registration.html.twig',
            [
                'activationData' => $activationData
            ]
        );

        $headers = 'From: ' . $setFrom . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";

        mail($setTo, $subject, $messageBody, $headers);

    }

    public function nativeSendUserRequestMail(UserRequest $userRequest)
    {

        $setFrom = getenv('ADMIN_EMAIL');
        $subject = "Новая заявка";
        $setTo = $setFrom;
        $messageBody = $this->templating->render(
            'emails/newUserRequest.html.twig',
            [
                'userRequest' => $userRequest
            ]
        );

        $headers = 'From: ' . $setFrom . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";

        mail($setTo, $subject, $messageBody, $headers);

    }

    public function nativeSendUserRequestEditedMail(UserRequest $userRequest)
    {
        $user = $this->tokenStorage->getToken()->getUser();

        $setFrom = getenv('ADMIN_EMAIL');
        $setTo = ($user === $userRequest->getUser()) ? $setFrom : $userRequest->getUser()->getEmail();
        $subject = "Уведомление о редактировании заявки";

        $messageBody = $this->templating->render(
        'emails/editUserRequest.html.twig',
        [
            'userRequest' => $userRequest,
        ]);
        $headers = 'From: ' . $setFrom . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=utf-8\r\n";

        mail($setTo, $subject, $messageBody, $headers);

    }


    public function sendMail(Message $message)
    {
        $email = (new \Swift_Message('Новое сообщение'))
            ->setFrom(getenv('ADMIN_EMAIL'));
        if ($message->getIsResponse()) $email->setTo($message->getOwner()->getEmail());
        else $email->setTo(getenv('ADMIN_EMAIL'));
        $email
            ->setBody(
                $this->templating->render(
                    'emails/newMessage.html.twig',
                    [
                        'message' => $message,
                    ]
                ), 'text/html'
            );

        $this->mailer->send($email);
    }

    public function sendActivationMail(array $activationData)
    {
        $message = (new \Swift_Message('Активация аккаунта'))
            ->setFrom(getenv('ADMIN_EMAIL'))
            ->setTo($activationData['userEmail'])
            ->setBody(
                $this->templating->render(
                    'emails/registration.html.twig',
                    [
                        'activationData' => $activationData
                    ]
                ), 'text/html'
            );

        $this->mailer->send($message);
    }




    public function sendUserRequestMail(UserRequest $userRequest)
    {
        $message = (new \Swift_Message('Новая заявка'))
            ->setFrom(getenv('ADMIN_EMAIL'))
            ->setTo(getenv('ADMIN_EMAIL'))
            ->setBody(
                $this->templating->render(
                    'emails/newUserRequest.html.twig',
                    [
                        'userRequest' => $userRequest
                    ]
                ), 'text/html'
            );

        $this->mailer->send($message);
    }

    public function sendUserRequestEditedMail(UserRequest $userRequest)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $message = (new \Swift_Message('Новая заявка'))
            ->setFrom(getenv('ADMIN_EMAIL'));

        if ($user === $userRequest->getUser())
            $message->setTo(getenv('ADMIN_EMAIL'));
        else
            $message->setTo($user->getEmail());

        $message->setBody($this->templating->render(
                    'emails/editUserRequest.html.twig',
                    [
                        'userRequest' => $userRequest,
                    ]
                ), 'text/html'
            );

        $this->mailer->send($message);
    }
}