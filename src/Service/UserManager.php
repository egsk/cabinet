<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 20.06.2018
 * Time: 10:13
 */

namespace App\Service;

Use App\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserManager
{
    private $user;

    private $router;

    private $requestStack;

    private $entityManager;

    private $tokenStorage;

    private $session;

    public function __construct(
        EntityManager $manager,
        RequestStack $requestStack,
        Router $route,
        TokenStorage $tokenStorage,
        Session $session
    ){
        $this->entityManager = $manager;
        $this->requestStack = $requestStack;
        $this->router = $route;
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function changeUserByTextParam(string $param, ?User $user = null): ?User
    {
        if ($user) $this->user = $user;
        elseif (!$this->user) return null;
        switch ($param){
            case 'confirm':
                $this->user->setRoles(['ROLE_USER']);
                break;
            case 'deny':
                $this->user->setRoles(['ROLE_DENIED']);
                break;
            default:
                return null;
        }
        return $this->user;
    }

    public function generateActivationData(?User $user = null): ?array
    {
        if ($user) $this->user = $user;
        elseif (!$this->user) return null;

        if (!$this->user->getConfirmToken()) return null;

        $activationData = [];
        $activationData['link'] = $this->router->generate('activateUser', [
            'token' => $this->user->getConfirmToken(),
        ], UrlGeneratorInterface::ABSOLUTE_URL);
        $activationData['homePage'] = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();
        $activationData['userEmail'] = $this->user->getEmail();
        return $activationData;
    }

    public function activateUser(?User $user = null): ?User
    {
        if ($user) $this->user = $user;
        elseif (!$this->user) return null;

        $this->user->setRoles(['ROLE_USER']);
        $this->user->setConfirmToken(null);
        $this->saveUser();

        $token = new UsernamePasswordToken(
            $this->user,
            null,
            'our_db_provider',
            $this->user->getRoles());
        $this->tokenStorage->setToken($token);
        $this->session->set('_security_main', serialize($token));

        return $this->user;
    }

    public function authorizeUser(User $user = null)
    {
        if ($user) $this->user = $user;
        elseif (!$this->user) return null;

        $token = new UsernamePasswordToken(
            $this->user,
            null,
            'our_db_provider',
            $this->user->getRoles());
        $this->tokenStorage->setToken($token);
        $this->session->set('_security_main', serialize($token));

        return $this->user;
    }

    public function saveUser(User $user = null)
    {
        if ($user) $this->user = $user;
        elseif (!$this->user) return null;

        $this->entityManager->persist($this->user);
        $this->entityManager->flush();

        return $this->user;
    }

    public function deleteUser(User $user = null)
    {
        if ($user) $this->user = $user;
        elseif (!$this->user) return null;

        $this->entityManager->remove($user);
        $this->entityManager->flush();

        return true;
    }

}