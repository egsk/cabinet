<?php

namespace App\Repository;

use App\Entity\Status;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Status|null find($id, $lockMode = null, $lockVersion = null)
 * @method Status|null findOneBy(array $criteria, array $orderBy = null)
 * @method Status[]    findAll()
 * @method Status[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatusRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Status::class);
    }

    public function getMessageCount($isResponse = false)
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.messages', 'm')
            ->addSelect('s, COUNT(s)')
//            ->andWhere('m.isResponse = :isResponse')
//            ->setParameter('isResponse', $isResponse)
            ->groupBy('s.id')
            ->getQuery()->getResult();
    }


    public function getActiveDialogsCount()
    {
        return $this->createQueryBuilder('s')
            ->leftJoin('s.dialogs', 'd')
            ->addSelect('COUNT(d)')
            ->groupBy('s.id')
            ->getQuery()->getResult();
    }

//    public function getAnswerCount()
//    {
//        return $this->createQueryBuilder('s')
//            ->leftJoin('s.answers', 'o')
//            ->addSelect('COUNT(o)')
//            ->groupBy('s.id')
//            ->getQuery()->getResult();
//    }

    public function getStatusByName(string $name)
    {
        return $this->createQueryBuilder('s')
            ->where('s.name = :name')
            ->setParameter('name', $name)
            ->getQuery()->getOneOrNullResult();
    }

//    /**
//     * @return Status[] Returns an array of Status objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Status
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
