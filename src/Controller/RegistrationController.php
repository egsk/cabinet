<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 19.06.2018
 * Time: 13:02
 */

namespace App\Controller;

use App\Form\UserType;
use App\Entity\User;
use App\Service\MailManager;
use App\Service\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Repository\UserRepository;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
/**
 * Class RegistrationController
 * @package App\Controller
 * @Route("/cabinet")
 */
class RegistrationController extends Controller
{
    /**
     * @Route("/register/", name="register")
     */
    public function register(Request $request, UserManager $userManager, MailManager $mailer)
    {
        if ($this->isGranted(['ROLE_DENIED'])) return $this->redirectToRoute('index');

        $user = new User();
        $form = $this->createForm(UserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $userManager->setUser($user);
            $userManager->saveUser();
            $userManager->authorizeUser();

            $mailer->nativeSendActivationMail($userManager->generateActivationData());

            return $this->redirectToRoute('status');
        }

        return $this->render(
            'views/register/register.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/activate/", name="activateUser")
     */
    public function activateUser(Request $request, UserRepository $repository, UserManager $userManager)
    {
        $user = $repository->findOneBy(['confirmToken' => $request->get('token')]);
        if (!$user) throw new UnauthorizedHttpException('Wrong token');
        $userManager->activateUser($user);
        return $this->redirectToRoute('dialog');
    }

    /**
     * @Route("/status/", name="status")
     * @IsGranted("ROLE_DENIED")
     */
    public function status()
    {
        if ($this->isGranted('ROLE_USER'))
            return $this->redirectToRoute('index');
        return $this->render('views/register/status.html.twig');
    }

}