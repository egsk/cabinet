<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 06.05.2018
 * Time: 19:12
 */

namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/cabinet")
 */
class SecurityController extends Controller
{
    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        if (!$this->getUser())
            return $this->redirectToRoute('login');

        elseif (!$this->isGranted('ROLE_USER'))
            return $this->redirectToRoute('status');

        elseif ($this->isGranted('ROLE_ADMIN'))
            return $this->redirectToRoute('adminIndex');

        else
            return $this->redirectToRoute('requests');

    }

    /**
     * @Route("/login", name="login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        if ($this->isGranted(['ROLE_DENIED'])) return $this->redirectToRoute('index');

        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();
        return $this->render('views/security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' =>$error
        ));
    }
}