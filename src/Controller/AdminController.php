<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 19.06.2018
 * Time: 15:57
 */
namespace App\Controller;
use App\Entity\Dialog;
use App\Entity\Status;
use App\Entity\User;
use App\Entity\UserRequest;
use App\Form\UserRequestAdminType;
use App\Repository\DialogRepository;
use App\Repository\MessageRepository;
use App\Repository\StatusRepository;
use App\Repository\UserRepository;
use App\Repository\UserRequestRepository;
use App\Service\DialogManager;
use App\Service\UserManager;
use App\Service\UserRequestManager;
use App\Util\UserGroups;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Message;
use App\Form\MessageType;
/**
 * Class AdminController
 * @package App\Controller
 * @Route("/cabinet/admin")
 * @IsGranted("ROLE_ADMIN")
 */
class AdminController  extends AbstractController
{
    /**
     * @Route("/", name="adminIndex")
     */
    public function admin()
    {
        return $this->redirectToRoute('requestsByStatus', ['name' => 'new']);
    }
    /**
     * @Route("/dialog/{id}/", requirements={"id"="\d+"}, name="adminDialog")
     */
    public function adminDialog(User $user,
                                DialogRepository $repository,
                                Request $request,
                                DialogManager $dialogManager,
                                MessageRepository $messageRepository)
    {
        $message = new Message();
        $dialog = $repository->findOneBy(['user' => $user ]);
        $userRequestId = $request->get('userRequestId');
        $messages = $messageRepository->findBy(['dialog' => $dialog], ['createdAt' => 'DESC']);
        if (!$dialog) $dialog = new Dialog();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $dialogManager->init($dialog, $message, $this->getUser(), $user, $userRequestId);
            $dialogManager->saveDialog();
            return $this->redirectToRoute('adminDialogs');
        }
        return $this->render('views/dialog.html.twig', [
            'dialog' => $dialog,
            'messages' => $messages,
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/dialogs/", name="adminDialogs")
     */
    public function adminDialogs(DialogRepository $dialogRepository)
    {
        $dialogs = $dialogRepository->findAll();
        return $this->render('views/admin/dialogs.html.twig', [
            'dialogs' => $dialogs
        ]);
    }
//    /**
//     * @Route("/dialog/{id}/delete", requirements={"id"="\d+"}, name="deleteDialog")
//     */
//    public function deleteDialog(Dialog $dialog, DialogManager $manager, DialogRepository $repository)
//    {
//        $manager->init($dialog);
//        $manager->deleteDialog();
//
//        $dialogs = $repository->findAll();
//
//        return $this->render('views/admin/dialogs.html.twig', [
//            'dialogs' => $dialogs
//        ]);
//
//    }
    /**
     * @Route("/profiles/", name="profiles")
     */
    public function profiles(UserRepository $repository)
    {
        $users = $repository->getUsers();
        $userGroups = new UserGroups($users);
//        $inactiveUsers = $userGroups->getUsersByRole('ROLE_INACTIVE');
        $activeUsers = $userGroups->getUsersByRole('ROLE_USER');
        $deniedUsers = $userGroups->getUsersByRole('ROLE_DENIED');
        return $this->render('views/admin/profiles.html.twig', [
            'userGroups' => [
//                'inactiveUsers' => $inactiveUsers,
                'activeUsers' => $activeUsers,
                'deniedUsers' => $deniedUsers
            ],
        ]);
    }
    /**
     * @Route("/profile/{id}/", name="adminProfile")
     */
    public function adminProfile(User $user)
    {
        if ($user->getHighestRole() === 'ROLE_ADMIN') return $this->redirectToRoute('profile');
        return $this->render('views/admin/profile.html.twig',[
            'user' => $user,
        ]);
    }
    /**
     * @Route("/confirm-user/{id}/", requirements={"id"="\d+"}, name="confirmUser")
     */
    public function confirmUser(User $user, UserManager $userManager)
    {
        if ($user->getHighestRole() === 'ROLE_ADMIN') return $this->redirectToRoute('profiles');
        $user->setRoles(['ROLE_USER']);
        $userManager->saveUser($user);
        return $this->redirectToRoute('profiles');
    }
    /**
     * @Route("/deny-user/{id}/", requirements={"id"="\d+"}, name="denyUser")
     */
    public function denyUser(User $user, UserManager $userManager)
    {
        if ($user->getHighestRole() === 'ROLE_ADMIN') return $this->redirectToRoute('profiles');
        $user->setRoles(['ROLE_DENIED']);
        $userManager->saveUser($user);
        return $this->redirectToRoute('profiles');
    }
    /**
     * @Route("/delete-user/{id}/", requirements={"id"="\d+"}, name="deleteUser")
     */
    public function deleteUser(UserManager $userManager, User $user)
    {
        if ($user->getHighestRole() === 'ROLE_ADMIN') return $this->redirectToRoute('profiles');
        $userManager->deleteUser($user);
        return $this->redirectToRoute('profiles');
    }
    /**
     * @Route("/requests/{name}/", name="requestsByStatus")
     */
    public function requestsByStatus(Status $status, UserRequestRepository $repository)
    {
        $requests = $repository->findRequestsByStatus($status);
        return $this->render('views/admin/requests.html.twig', [
            'requests' => $requests,
        ]);
    }
    /**
     * @Route("/request/{id}/", requirements={"id"="\d+"}, name="adminUserRequest")
     */
    public function adminUserRequest(UserRequest $userRequest)
    {
        return $this->render('views/request.html.twig', [
            'request' => $userRequest,
        ]);
    }
    /**
     * @Route("/request/{id}/set-status/", requirements={"id"="\d+"}, name="adminUserRequestStatus")
     */
    public function setStatusToUserRequest(UserRequest $userRequest, Request $request, UserRequestManager $manager)
    {
        $statusName = $request->get('status');
        $manager->init($userRequest, $this->getUser());
        $manager->saveUserRequest($statusName);
        $userRequest->setSendEmail(true);
        $referer = $request->headers->get('referer');
        if ($referer)
            return new RedirectResponse($request->headers->get('referer'));
        return $this->redirectToRoute('requestsByStatus', ['name' => 'new']);
    }
    /**
     * @Route("/request/{id}/delete/", requirements={"id"="\d+"}, name="adminUserRequestDelete")
     */
    public function userRequestDelete(UserRequest $userRequest, UserRequestManager $manager)
    {
        $manager->init($userRequest, $this->getUser());
        $manager->delete();
        return $this->redirectToRoute('requestsByStatus', ['name' => 'new']);
    }
    /**
     * @Route("/request/{id}/edit/", requirements={"id"="\d+"}, name="adminUserRequestEdit")
     */
    public function adminUserRequestEdit(
        UserRequest $userRequest, UserRequestManager $userRequestManager, Request $request)
    {
        $form = $this->createForm(UserRequestAdminType::class, $userRequest);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid())
        {
            $userRequestManager->init($userRequest, $userRequest->getUser());
            $userRequestManager->saveUserRequest('refreshed');
            return $this->redirectToRoute('userRequest', ['id' => $userRequest->getId()]);
        }
        return $this->render('views/forms/userRequestAnswerForm.html.twig', [
            'form' => $form->createView(),
            'request' => $userRequest,
        ]);
    }
}