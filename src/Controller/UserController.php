<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 19.06.2018
 * Time: 12:36
 */

namespace App\Controller;

use App\Entity\Dialog;
use App\Entity\Message;
use App\Entity\UserRequest;
use App\Form\ChangePasswordType;
use App\Form\MessageType;
use App\Form\UserRequestType;
use App\Repository\MessageRepository;
use App\Repository\UserRequestRepository;
use App\Service\ArchiveDownloader;
use App\Service\DialogManager;
use App\Service\LinkManager;
use App\Service\MessageManager;
use App\Service\UserManager;
use App\Service\UserRequestManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Link;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use App\Form\EditProfileType;

/**
 * @IsGranted("ROLE_USER")
 * @Route("/cabinet")
 */
class UserController extends AbstractController
{

    /**
     * @Route("/dialog/", name="dialog")
     */
    public function dialog(Request $request,
                           DialogManager $dialogManager,
                           MessageRepository $messageRepository)
    {
        if ($this->isGranted('ROLE_ADMIN')) return $this->redirectToRoute('adminIndex');

        $userRequestId = $request->get('userRequestId');
        $dialog = $this->getUser()->getDialog();

        $messages = $messageRepository->findBy(['dialog' => $dialog], ['createdAt' => 'DESC']);

        if (!$dialog){
            $dialog = new Dialog();
            $dialog->setUser($this->getUser());
        }

        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $dialogManager->init($dialog, $message, $this->getUser(), null, $userRequestId);
            $dialogManager->saveDialog();
            return $this->redirectToRoute('profile');
        }

        return $this->render('views/dialog.html.twig', [
            'dialog' => $dialog,
            'messages' => $messages,
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/request/new/", name="newRequest")
     */
    public function createRequest(Request $request, UserRequestManager $userRequestManager)
    {
        $userRequest = new UserRequest();

        $form = $this->createForm(UserRequestType::class, $userRequest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $isPublished = !$form->get('saveDraft')->isClicked();
            $userRequest->setPublished($isPublished);
            $userRequestManager->init($userRequest, $this->getUser());
            $userRequestManager->saveUserRequest();
            return $this->redirectToRoute('requests');
        }
        return $this->render('views/forms/userRequestForm.html.twig', [
            'form' => $form->createView(),
            'request' => $userRequest,
        ]);

    }
    /**
     * @Route("/request/{id}/edit/", requirements={"id"="\d+"}, name="editRequest")
     */
    public function editUserRequest(UserRequest $userRequest, Request $request, UserRequestManager $userRequestManager)
    {
        if (!($this->getUser() === $userRequest->getUser()))
            throw new AccessDeniedHttpException('Эта заявка принадлежит не вам.');

        $form = $this->createForm(UserRequestType::class, $userRequest);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {
            $isPublished = !$form->get('saveDraft')->isClicked();
            $userRequest->setPublished($isPublished);
            $userRequestManager->init($userRequest, $this->getUser());
            $userRequestManager->saveUserRequest();
            return $this->redirectToRoute('requests');
        }
        return $this->render('views/forms/userRequestForm.html.twig', [
            'form' => $form->createView(),
            'request' => $userRequest,
        ]);
    }

    /**
     * @Route("/request/{id}/", requirements={"id"="\d+"}, name="userRequest")
     */
    public function userRequest(UserRequest $userRequest)
    {
        if (!($this->getUser() === $userRequest->getUser()) && !$this->isGranted('ROLE_ADMIN'))
            throw new AccessDeniedHttpException('Эта заявка принадлежит не вам.');
        return $this->render('views/request.html.twig', [
            'request' => $userRequest,
        ]);

    }

    /**
     * @Route("/download-archive/{id}/", requirements={"id"="\d+"}, name="downloadArchive")
     */
    public function downloadArchive(UserRequest $userRequest)
    {
        if (!($this->isGranted('ROLE_ADMIN') || $this->getUser() === $userRequest->getUser()))
            throw new AccessDeniedHttpException('Недостаточно прав для выполнения действия');

        $archive = new ArchiveDownloader($userRequest);

        $response = new BinaryFileResponse($archive->createArchive());
        $response->deleteFileAfterSend(true);
        $response->setContentDisposition('attachment', $userRequest->getObjectName() . '.zip');

        return $response;
    }

    /**
     * @Route("/requests/", name="requests")
     */
    public function userRequests()
    {
        $requests = $this->getUser()->getUserRequests();
        return $this->render('views/user/requests.html.twig', [
            'requests' => $requests,
        ]);
    }


    /**
     * @Route("/delete-file/{id}", requirements={"id"="\d+"}, name="deleteFile")
     */
    public function deleteFileFromUserRequest(Link $link, LinkManager $linkManager, Request $request)
    {
        if (!(($this->isGranted('ROLE_ADMIN') )|| ($link->getOwner() === $this->getUser()) )){
            throw new AccessDeniedHttpException('Недостаточно прав для выполнения действия');
        }

        $linkManager->init($link);

        $linkManager->deleteLink();

        $referer = $request->headers->get('referer');

        if ($referer)
            return new RedirectResponse($request->headers->get('referer'));

        return $this->redirectToRoute('profile');

    }

    /**
     * @Route("/download/{id}", requirements={"id"="\d+"}, name="download")
     */
    public function download(Link $link, LinkManager $linkManager)
    {
        if (!$this->isGranted('ROLE_ADMIN') &&
            !($link->getOwner() === $this->getUser()))
            throw new NotFoundHttpException(
                'Файл не существует, или у вас на хватает прав на его скачивание
                ');
        $linkManager->init($link);

        return $this->file($linkManager->getRealPath(), $linkManager->getRealName());
    }

    /**
     * @Route("/profile/", name="profile")
     */
    public function profile()
    {
        return $this->render('views/user/profile.html.twig', [
            'user' => $this->getUser(),
        ]);
    }

    /**
     * @Route("/profile/edit/", name="editProfile")
     */
    public function editProfile(Request $request, UserManager $userManager)
    {
        $user = $this->getUser();
        $form = $this->createForm(EditProfileType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $userManager->setUser($user);
            $userManager->saveUser();
            $userManager->authorizeUser();

            return $this->redirectToRoute('profile');
        }

        return $this->render(
            'views/user/editProfile.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/profile/change-password/", name="changePassword")
     */
    public function changePassword(Request $request, UserManager $userManager)
    {
        $user = $this->getUser();
        $form = $this->createForm(ChangePasswordType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $userManager->setUser($user);
            $userManager->saveUser();
            $userManager->authorizeUser();

            return $this->redirectToRoute('profile');
        }

        return $this->render(
            'views/user/changePassword.html.twig',
            array('form' => $form->createView())
        );
    }

    /**
     * @Route("/message/delete/{id}", name="deleteMessage")
     */
    public function deleteMessage(Message $message, Request $request, MessageManager $messageManager)
    {
        if (!(
            ($this->isGranted('ROLE_ADMIN')) ||
            (!$message->getIsResponse() && ($message->getOwner() == $this->getUser()))
        ))
            throw new AccessDeniedHttpException('Недостаточно прав для действия');

        $messageManager->deleteMessage($message);

        $referer = $request->headers->get('referer');

        if ($referer)
            return new RedirectResponse($request->headers->get('referer'));

        return $this->redirectToRoute('dialog');

    }
}