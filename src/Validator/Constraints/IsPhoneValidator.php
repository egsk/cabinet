<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 20.06.2018
 * Time: 21:03
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class IsPhoneValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        $pattern =
            "`^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$`";
        if (!preg_match($pattern, $value, $matches)){
            $this->context->buildViolation($constraint->message)
                ->setParameter('{{ string }}', $value)
                ->addViolation();
        }
    }

}