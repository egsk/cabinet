<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 20.06.2018
 * Time: 21:02
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsPhone extends Constraint
{
    public $message = "Пожалуйста, введите валидный номер телефона";

}