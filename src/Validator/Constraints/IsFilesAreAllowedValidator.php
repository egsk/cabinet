<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 05.07.2018
 * Time: 07:15
 */
namespace App\Validator\Constraints;
use App\Entity\UserRequest;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
class IsFilesAreAllowedValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if ($value)
            foreach ($value as $file){
                if ((!$file instanceof UploadedFile) ||
                    (!in_array($file->getClientOriginalExtension(), UserRequest::allowedExtensions)) ||
                    ($file->getSize() > 85000000 ))
                    $this->context->buildViolation($constraint->message)
                        ->setParameter('{{ string }}', 'Неправильный тип файла')
                        ->addViolation();
            }
    }
}