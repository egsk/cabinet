<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 05.07.2018
 * Time: 07:15
 */

namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class IsFilesAreAllowed extends Constraint
{
    public $message = "Ошибка при отправке заявки: Вы попытались загрузить недопустимый тип файла(ов)";

}