<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 05.05.2018
 * Time: 13:16
 */

namespace App\Form;


use App\Entity\MappedSuperclass\Talk;
use App\Entity\Message;
use App\Entity\UserRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class UserRequestAdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('objectName', TextType::class, array(
                'label' => 'Название объекта',
                'attr' => [
                    'placeholder' => 'Введите название объекта'
                ]
            ))
            ->add('adminContractFiles', CollectionType::class, [
                'label' =>
                    'Договор',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,
            ])
            ->add('billFiles', CollectionType::class, [
                'label' =>
                    'Счет',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,
            ])
            ->add('remarkFiles', CollectionType::class, [
                'label' =>
                    'Замечания',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,
            ])
            ->add('decisionFiles', CollectionType::class, [
                'label' =>
                    'Заключение',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,
            ])
            ->add('sendEmail', CheckboxType::class, [
                'label' => 'Отправить E-mail уведомление об изменении заявки',
                'required' => false,
            ])            ->add('save', SubmitType::class, [
                'label' => 'Сохранить как ответ',
                'attr' => [
                    'class' => 'btn btn-raised btn-primary btn-lg'
                ]
            ])
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserRequest::class
        ));
    }
}