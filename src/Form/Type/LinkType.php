<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 23.06.2018
 * Time: 07:36
 */

namespace App\Form\Type;


use App\Entity\Link;
use App\Repository\LinkRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LinkType extends AbstractType
{
    /**
     * @var Link
     */
    private $link;

    public function __construct(LinkRepository $linkRepository)
    {
        $this->link = $linkRepository;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefault(
            'label', $this->link->getFilename()
        );
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
    }

    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        parent::buildView($view, $form, $options);
    }

    public function getParent()
    {
        return CheckboxType::class;
    }


}