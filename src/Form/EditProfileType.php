<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 23.06.2018
 * Time: 06:01
 */

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class EditProfileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullName', TextType::class, [
                'label' => 'Изменить имя',
                'attr' => [
                    'placeholder' => 'Полное имя'
                ]
            ])
            ->add('company', TextType::class, [
                'label' => 'Изменить название компании',
                'attr' => [
                    'placeholder' => 'Компания'
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => 'Изменить номер телефона',
                'attr' => [
                    'placeholder' => 'Номер телефона'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}