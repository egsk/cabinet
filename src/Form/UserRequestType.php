<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 05.05.2018
 * Time: 13:16
 */

namespace App\Form;


use App\Entity\MappedSuperclass\Talk;
use App\Entity\Message;
use App\Entity\UserRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class UserRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('objectName', TextType::class, array(
                'label' => 'Название объекта',
                'attr' => [
                    'placeholder' => 'Введите название объекта'
                ]
            ))
            ->add('contractFiles', CollectionType::class, [
                'label' => 'Заявление, приложения, договорные документы',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('isd1Files', CollectionType::class, [
                'label' => 'Исходно-разрешительная документация',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp2Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 2.',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp3Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 3.',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp4Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 4.',

                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp5Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 5.',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp6Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 6.',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp7Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 7.',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp8Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 8.',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp9Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 9.',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('locs10Files', CollectionType::class, [
                'label' =>
                    'Линейные объекты капитального строительства. Раздел 10.',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp101Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 10_1',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,

            ])
            ->add('oksp11Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 11',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,
            ])
            ->add('oksp12Files', CollectionType::class, [
                'label' =>
                    'Объекты капитального строительства производственного и непроизводственного назначения. Раздел 12',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,
                'entry_options' => array(
                    'attr' => array('class' => 'file-box')
                )
            ])
            ->add('correctionFiles', CollectionType::class, [
                'label' => 'Ответы на замечания',
                'required' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,
                'entry_options' => array(
                    'attr' => array('class' => 'file-box')
                )
            ])
            ->add('sendEmail', CheckboxType::class, [
                'label' => 'Отправить E-mail уведомление об изменении заявки',
                'required' => false,
            ])
            ->add('save', SubmitType::class, [
                'label' => 'Сохранить',
                'attr' => [
                    'class' => 'btn btn-raised btn-primary btn-lg'
                ]
            ])
            ->add('saveDraft', SubmitType::class, [
                'label' => 'Сохранить черновик',
                'attr' => [
                    'class' => 'btn btn-raised btn-primary btn-lg btn-alter'
                ]
            ])
        ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => UserRequest::class
        ));
    }
}