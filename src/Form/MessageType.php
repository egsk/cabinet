<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 05.05.2018
 * Time: 13:16
 */

namespace App\Form;


use App\Entity\MappedSuperclass\Talk;
use App\Entity\Message;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', TextareaType::class, array(
                'label' => false,
                'attr' => array(
                    'placeholder' => 'Ваше сообщение'
                ),
            ))
            ->add('files', CollectionType::class, [
                'label' => false,
                'entry_type' => FileType::class,
                'allow_add' => true,
                'prototype' => true,
                'required' => false,
                'entry_options' => array(
                    'attr' => array('class' => 'file-box')
                )
            ]);
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Message::class
        ));
    }
}