<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 19.06.2018
 * Time: 13:05
 */
namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Адрес эл. почты (используется для входа на сайт):',
                'attr' => [
                    'placeholder' => 'Введите адрес эл. почты',
                ]
            ])
            ->add('fullName', TextType::class , [
                'label' => 'Полное имя:',
                'attr' => [
                    'placeholder' => 'Введите ваше полное имя:',
                ]
            ])
            ->add('company', TextType::class, [
                'label' => 'Название компании:',
                'attr' => [
                    'placeholder' => 'Введите название компании:',
                ]
            ])
            ->add('phone', TextType::class, [
                'label' => 'Телефонный номер:',
                'attr' => [
                    'placeholder' => 'Введите ваш номер телефона:',
                ]
            ])
            ->add('addressCustom', TextType::class, [
                'label' => false,
                'required' => false,
                'attr' => [
                    'placeholder' => 'Адрес'
                ]
            ])
            ->add('plainPassword', RepeatedType::class, array(
                'type' => PasswordType::class,
                'first_options'  => array('label' => 'Пароль:', 'attr' => ['placeholder' => 'Введите пароль']),
                'second_options' => array('label' => 'Пароль повторно:', 'attr' => ['placeholder' => 'Повторите ввод пароля']),
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}