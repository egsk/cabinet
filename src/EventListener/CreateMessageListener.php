<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 26.06.2018
 * Time: 04:42
 */

namespace App\EventListener;

use App\Entity\Message;
use App\Entity\UserRequest;
use App\Service\MailManager;
use Doctrine\ORM\Event\LifecycleEventArgs;

class CreateMessageListener
{
    private $mailManager;

    public function __construct(MailManager $mailManager)
    {
        $this->mailManager = $mailManager;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Message){
            $this->mailManager->nativeSendMail($entity);
        }
    }

}