<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 10.05.2018
 * Time: 04:50
 */

namespace App\EventListener;


use App\Entity\Link;
use Doctrine\ORM\Event\LifecycleEventArgs;

class RemoveLinkListener
{
    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Link){
            $path  = Link::PATH . $entity->getTextLink();
            if (file_exists($path))
                unlink($path);
        }
    }
}