<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 10.05.2018
 * Time: 04:50
 */

namespace App\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;

class CreatedAtListener
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (method_exists($entity, 'setCreatedAt')
            && method_exists($entity, 'getCreatedAt')
            && $entity->getCreatedAt() === null){
            date_default_timezone_set('Europe/Moscow');
            $entity->setCreatedAt(new \DateTime('now'));
        }
        if (method_exists($entity, 'setUpdatedAt')){
            date_default_timezone_set('Europe/Moscow');
            $entity->setUpdatedAt(new \DateTime('now'));
        }
    }
}