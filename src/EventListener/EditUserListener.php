<?php
/**
 * Created by PhpStorm.
 * User: DWT
 * Date: 09.05.2018
 * Time: 14:52
 */

namespace App\EventListener;


use App\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class EditUserListener
{
    private $encoder;
    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof User)
            return;
        $plainPassword = $entity->getPlainPassword();
        $entity->eraseCredentials();

        if (!$entity->getConfirmToken() && $entity->getRoles() === ['ROLE_INACTIVE']){
            $confirmToken = $this->encoder->encodePassword($entity, uniqid(rand()));
            $entity->setConfirmToken($confirmToken);
        }

        if (!$plainPassword) return;
        $password = $this->encoder->encodePassword($entity, $plainPassword);
        $entity->setPassword($password);
    }
}