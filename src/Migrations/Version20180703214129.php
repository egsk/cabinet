<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180703214129 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE link ADD admin_contracts_id INT DEFAULT NULL, ADD bills_id INT DEFAULT NULL, ADD decisions_id INT DEFAULT NULL, ADD remarks_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F15D19E789 FOREIGN KEY (admin_contracts_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F12ABC37A4 FOREIGN KEY (bills_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1B8C9DB58 FOREIGN KEY (decisions_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1296FF2C5 FOREIGN KEY (remarks_id) REFERENCES user_request (id)');
        $this->addSql('CREATE INDEX IDX_36AC99F15D19E789 ON link (admin_contracts_id)');
        $this->addSql('CREATE INDEX IDX_36AC99F12ABC37A4 ON link (bills_id)');
        $this->addSql('CREATE INDEX IDX_36AC99F1B8C9DB58 ON link (decisions_id)');
        $this->addSql('CREATE INDEX IDX_36AC99F1296FF2C5 ON link (remarks_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F15D19E789');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F12ABC37A4');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1B8C9DB58');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1296FF2C5');
        $this->addSql('DROP INDEX IDX_36AC99F15D19E789 ON link');
        $this->addSql('DROP INDEX IDX_36AC99F12ABC37A4 ON link');
        $this->addSql('DROP INDEX IDX_36AC99F1B8C9DB58 ON link');
        $this->addSql('DROP INDEX IDX_36AC99F1296FF2C5 ON link');
        $this->addSql('ALTER TABLE link DROP admin_contracts_id, DROP bills_id, DROP decisions_id, DROP remarks_id');
    }
}
