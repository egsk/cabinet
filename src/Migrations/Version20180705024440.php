<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180705024440 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1F8697D13');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP INDEX IDX_36AC99F1F8697D13 ON link');
        $this->addSql('ALTER TABLE link DROP comment_id');
        $this->addSql('ALTER TABLE message ADD user_request_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307FE5197E49 FOREIGN KEY (user_request_id) REFERENCES user_request (id)');
        $this->addSql('CREATE INDEX IDX_B6BD307FE5197E49 ON message (user_request_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, user_request_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, text_content LONGTEXT NOT NULL COLLATE utf8mb4_unicode_ci, created_at DATETIME NOT NULL, INDEX IDX_9474526CE5197E49 (user_request_id), INDEX IDX_9474526C7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user_list (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CE5197E49 FOREIGN KEY (user_request_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD comment_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1F8697D13 FOREIGN KEY (comment_id) REFERENCES comment (id)');
        $this->addSql('CREATE INDEX IDX_36AC99F1F8697D13 ON link (comment_id)');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307FE5197E49');
        $this->addSql('DROP INDEX IDX_B6BD307FE5197E49 ON message');
        $this->addSql('ALTER TABLE message DROP user_request_id');
    }
}
