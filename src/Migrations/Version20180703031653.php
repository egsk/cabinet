<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180703031653 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE dialog (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, status_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_4561D862A76ED395 (user_id), INDEX IDX_4561D8626BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_list (id INT AUTO_INCREMENT NOT NULL, full_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, phone VARCHAR(255) NOT NULL, company VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, roles JSON NOT NULL, confirm_token VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_3E49B4D1E7927C74 (email), UNIQUE INDEX UNIQ_3E49B4D1444F97DD (phone), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_request (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, status_id INT DEFAULT NULL, object_name VARCHAR(255) NOT NULL, published TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_639A9195A76ED395 (user_id), INDEX IDX_639A91956BF700BD (status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE link (id INT AUTO_INCREMENT NOT NULL, message_id INT DEFAULT NULL, isd1_id INT DEFAULT NULL, oksp2_id INT DEFAULT NULL, oksp3_id INT DEFAULT NULL, oksp4_id INT DEFAULT NULL, oksp5_id INT DEFAULT NULL, oksp6_id INT DEFAULT NULL, oksp7_id INT DEFAULT NULL, oksp8_id INT DEFAULT NULL, oksp9_id INT DEFAULT NULL, locs10_id INT DEFAULT NULL, oksp101_id INT DEFAULT NULL, oksp11_id INT DEFAULT NULL, oksp12_id INT DEFAULT NULL, request_correction_id INT DEFAULT NULL, owner_id INT NOT NULL, contract_id INT DEFAULT NULL, comment_id INT DEFAULT NULL, text_link VARCHAR(255) NOT NULL, filename VARCHAR(255) NOT NULL, INDEX IDX_36AC99F1537A1329 (message_id), INDEX IDX_36AC99F1B3D1FB4C (isd1_id), INDEX IDX_36AC99F1521EC6C9 (oksp2_id), INDEX IDX_36AC99F1EAA2A1AC (oksp3_id), INDEX IDX_36AC99F177759915 (oksp4_id), INDEX IDX_36AC99F1CFC9FE70 (oksp5_id), INDEX IDX_36AC99F1DD7C519E (oksp6_id), INDEX IDX_36AC99F165C036FB (oksp7_id), INDEX IDX_36AC99F13DA326AD (oksp8_id), INDEX IDX_36AC99F1851F41C8 (oksp9_id), INDEX IDX_36AC99F1971788AB (locs10_id), INDEX IDX_36AC99F16C7781C2 (oksp101_id), INDEX IDX_36AC99F173263391 (oksp11_id), INDEX IDX_36AC99F161939C7F (oksp12_id), INDEX IDX_36AC99F11CBD78D4 (request_correction_id), INDEX IDX_36AC99F17E3C61F9 (owner_id), INDEX IDX_36AC99F12576E0FD (contract_id), INDEX IDX_36AC99F1F8697D13 (comment_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, dialog_id INT DEFAULT NULL, status_id INT DEFAULT NULL, owner_id INT NOT NULL, text LONGTEXT NOT NULL, is_response TINYINT(1) NOT NULL, INDEX IDX_B6BD307F5E46C4E2 (dialog_id), INDEX IDX_B6BD307F6BF700BD (status_id), INDEX IDX_B6BD307F7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE comment (id INT AUTO_INCREMENT NOT NULL, user_request_id INT DEFAULT NULL, owner_id INT DEFAULT NULL, text_content LONGTEXT NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_9474526CE5197E49 (user_request_id), INDEX IDX_9474526C7E3C61F9 (owner_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, content VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_7B00651C5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE dialog ADD CONSTRAINT FK_4561D862A76ED395 FOREIGN KEY (user_id) REFERENCES user_list (id)');
        $this->addSql('ALTER TABLE dialog ADD CONSTRAINT FK_4561D8626BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE user_request ADD CONSTRAINT FK_639A9195A76ED395 FOREIGN KEY (user_id) REFERENCES user_list (id)');
        $this->addSql('ALTER TABLE user_request ADD CONSTRAINT FK_639A91956BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1537A1329 FOREIGN KEY (message_id) REFERENCES message (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1B3D1FB4C FOREIGN KEY (isd1_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1521EC6C9 FOREIGN KEY (oksp2_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1EAA2A1AC FOREIGN KEY (oksp3_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F177759915 FOREIGN KEY (oksp4_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1CFC9FE70 FOREIGN KEY (oksp5_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1DD7C519E FOREIGN KEY (oksp6_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F165C036FB FOREIGN KEY (oksp7_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F13DA326AD FOREIGN KEY (oksp8_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1851F41C8 FOREIGN KEY (oksp9_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1971788AB FOREIGN KEY (locs10_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F16C7781C2 FOREIGN KEY (oksp101_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F173263391 FOREIGN KEY (oksp11_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F161939C7F FOREIGN KEY (oksp12_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F11CBD78D4 FOREIGN KEY (request_correction_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F17E3C61F9 FOREIGN KEY (owner_id) REFERENCES user_list (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F12576E0FD FOREIGN KEY (contract_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1F8697D13 FOREIGN KEY (comment_id) REFERENCES comment (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F5E46C4E2 FOREIGN KEY (dialog_id) REFERENCES dialog (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F6BF700BD FOREIGN KEY (status_id) REFERENCES status (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user_list (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526CE5197E49 FOREIGN KEY (user_request_id) REFERENCES user_request (id)');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C7E3C61F9 FOREIGN KEY (owner_id) REFERENCES user_list (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F5E46C4E2');
        $this->addSql('ALTER TABLE dialog DROP FOREIGN KEY FK_4561D862A76ED395');
        $this->addSql('ALTER TABLE user_request DROP FOREIGN KEY FK_639A9195A76ED395');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F17E3C61F9');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F7E3C61F9');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C7E3C61F9');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1B3D1FB4C');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1521EC6C9');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1EAA2A1AC');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F177759915');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1CFC9FE70');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1DD7C519E');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F165C036FB');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F13DA326AD');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1851F41C8');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1971788AB');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F16C7781C2');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F173263391');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F161939C7F');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F11CBD78D4');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F12576E0FD');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526CE5197E49');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1537A1329');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1F8697D13');
        $this->addSql('ALTER TABLE dialog DROP FOREIGN KEY FK_4561D8626BF700BD');
        $this->addSql('ALTER TABLE user_request DROP FOREIGN KEY FK_639A91956BF700BD');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F6BF700BD');
        $this->addSql('DROP TABLE dialog');
        $this->addSql('DROP TABLE user_list');
        $this->addSql('DROP TABLE user_request');
        $this->addSql('DROP TABLE link');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE comment');
        $this->addSql('DROP TABLE status');
    }
}
